<?php

/**
 * Administration settings page
 */
function oe_zoho_admin($form, &$form_state) {
    require_once libraries_get_path('zoho') . '/Zoho.class.php';

    $user = variable_get('zoho_user', NULL);
    $pass = variable_get('zoho_pass', NULL);
    $portal = variable_get('zoho_portal', NULL);
    $authtoken = variable_get('zoho_authtoken', NULL);

    if (is_null($authtoken) || empty($authtoken)) {
        $zoho = new Zoho($user, $pass, $portal);
        variable_set('zoho_authtoken', $zoho->authtoken);
    } else {
        $zoho = new Zoho($user, $pass, $portal, $authtoken);
    }
    
    $form = array();
    $form['zoho_user'] = array(
        '#type' => 'textfield',
        '#title' => t('Zoho Username'),
        '#default_value' => $user,
        '#required' => TRUE,
    );
    if (is_null($authtoken) || empty($authtoken)) {
        $form['zoho_pass'] = array(
            '#type' => 'password',
            '#title' => t('Zoho Password'),
            '#default_value' => $pass,
            '#required' => TRUE,
        );
    } else {
        $form['zoho_authtoken'] = array(
            '#type' => 'textfield',
            '#title' => t('Zoho Authtoken'),
            '#default_value' => $authtoken,
            '#required' => TRUE,
        );
    }


    $portals_json = $zoho->getPortals();
    $portals = $portals_json->response->result->PortalDetails[0]->PortalDetail->PortalName;
    $portal_options = array();
    foreach ($portals as $portalObj) {
        $portal_options[$portalObj->portal] = $portalObj->portal_compname;
        if ($portalObj->portal_isdefault && is_null($portal))
            $portal = $portalObj->portal;
    }
    $portal_description = t('Select the portal to work from.');
    $form['zoho_portal'] = array(
        '#type' => 'select',
        '#title' => t('Portal'),
        '#options' => $portal_options,
        '#default_value' => $portal,
        '#description' => $portal_description,
    );

    $projects_json = $zoho->getProjects();
    $projects = $projects_json->response->result->ProjectDetails;
    $project_options = array();
    foreach ($projects as $projectObj) {
        $project_options[$projectObj->ProjectDetail->project_id] = $projectObj->ProjectDetail->project_name;
    }
    $zoho_coursedev_project = variable_get('zoho_coursedev_project', key($project_options));
    $zoho_coursemaintenance_project = variable_get('zoho_coursemaintenance_project', key($project_options));

    $coursedev_description = t('Select the project to work from.');
    $form['zoho_coursedev_project'] = array(
        '#type' => 'select',
        '#title' => t('Course Development Project'),
        '#options' => $project_options,
        '#default_value' => $zoho_coursedev_project,
        '#description' => $coursedev_description,
    );
    $coursemaintenance_description = t('Select the project to work from.');
    $form['zoho_coursemaintenance_project'] = array(
        '#type' => 'select',
        '#title' => t('Course Maintenance Project'),
        '#options' => $project_options,
        '#default_value' => $zoho_coursemaintenance_project,
        '#description' => $coursemaintenance_description,
    );

    return system_settings_form($form);
}
